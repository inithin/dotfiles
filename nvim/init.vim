call plug#begin()

Plug 'arcticicestudio/nord-vim'
Plug 'vim-airline/vim-airline'
Plug 'mhinz/vim-startify'

call plug#end()

colorscheme nord

