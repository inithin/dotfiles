#Pfetch
pfetch

#Ps1 and some stuff.

# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

#Set some zsh options
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic
setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt interactive_comments

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# add scripts to path
export PATH="/home/nithin/.local/bin:$PATH"

#Aliases
# Verbosity and settings that you pretty much just always are going to want.
alias \
	cp="cp -iv" \
	ls="ls --color=auto -hF"\
	l="ls"\
	ll="ls -lahF"\
	grep="grep --color=auto"\
	dmesg="dmesg --color=auto"\
	mv="mv -iv" \
	rm="rm -vI" \
	mkd="mkdir -pv" \
	yt="youtube-dl --add-metadata -i" \
	up="paru" \
	v="nvim"\
	sv="sudo nvim"\
	mc="setsid gamemoderun DRI_PRIME=1 minecraft-launcher" \
	wall="feh --bg-scale --no-fehbg --randomize /home/nithin/.config/walls 2>&1" \
	cfd="cd ~/.config/dwm ; v config.h"\
	cft="cd ~/.config/alacritty; v alacritty.yml"\
	yta="yt -x -f bestaudio/best" 

## Set stuff up
export BROWSER="brave"
export TERMINAL="st"
export EDITOR="nvim"
export MANPAGER="bat"

#Source some features.
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
